=============================
Dynamic Meta Models Tools in Python
=============================

.. image:: https://badge.fury.io/py/pydmmt.png
    :target: http://badge.fury.io/py/pydmmt

.. image:: https://travis-ci.org/Lordmzn/pydmmt.svg?branch=master
    :target: https://travis-ci.org/lordmzn/pydmmt

.. image:: https://pypip.in/d/pydmmt/badge.png
    :target: https://pypi.python.org/pypi/pydmmt


Provides tools to construct programs that simulate dynamic systems of equations in Python.


Features
--------

* TODO

